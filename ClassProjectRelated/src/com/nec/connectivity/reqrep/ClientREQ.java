package com.nec.connectivity.reqrep;

import java.io.File;
import java.io.IOException;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClientREQ {
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
        Context context = ZMQ.context(1);

        //  Socket to talk to server
        Socket requester = context.socket(ZMQ.REQ);
        requester.connect("tcp://localhost:5559");
        
        System.out.println("launch and connect client.");

//        for (int request_nbr = 0; request_nbr < 10; request_nbr++) {
        int request_nbr =0;
        
        ObjectMapper mapper = new  ObjectMapper();
        Object object = mapper.readValue(new File("student.json"), Object.class);
        byte[] dataByte = mapper.writeValueAsBytes(object);
            requester.send(dataByte, 0);
            String reply = requester.recvStr(0);
            System.out.println("Received reply " + request_nbr + " [" + reply + "]");
//        }
        
        //  We never get here but clean up anyhow
        requester.close();
        context.term();
    }

}
