package com.nec.connectivity.reqrep;

 import java.io.IOException;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nec.json.Student;

public class ServerREP {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		Context context = ZMQ.context(1);

		// Socket to talk to clients
		Socket responder = context.socket(ZMQ.REP);
		responder.bind("tcp://localhost:5559");

		System.out.println("launch and connect server.");

		while (!Thread.currentThread().isInterrupted()) {
			// Wait for next request from client
			byte[] request = responder.recv(0);
			ObjectMapper mapper = new ObjectMapper();
			Student student = mapper.readValue(request, Student.class);
//			String string = new String(request);
			System.out.println("Received request: [" + student + "].");

			// Do some 'work'
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// Send reply back to client
			responder.send("World".getBytes(), 0);
		}

		// We never get here but clean up anyhow
		responder.close();
		context.term();
	}

}
