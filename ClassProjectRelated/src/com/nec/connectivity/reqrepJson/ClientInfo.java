package com.nec.connectivity.reqrepJson;

import java.util.Map;

public class ClientInfo {
	String ip;
	Map<String, String> clientInfo;
	int sourcePoint;
	int destinationPoint;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Map<String, String> getClientInfo() {
		return clientInfo;
	}

	public void setClientInfo(Map<String, String> clientInfo) {
		this.clientInfo = clientInfo;
	}

	public int getSourcePoint() {
		return sourcePoint;
	}

	public void setSourcePoint(int sourcePoint) {
		this.sourcePoint = sourcePoint;
	}

	public int getDestinationPoint() {
		return destinationPoint;
	}

	public void setDestinationPoint(int destinationPoint) {
		this.destinationPoint = destinationPoint;
	}
}
