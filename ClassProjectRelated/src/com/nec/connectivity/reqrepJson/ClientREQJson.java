package com.nec.connectivity.reqrepJson;

import java.io.File;
import java.io.IOException;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClientREQJson {
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
        Context context = ZMQ.context(1);

        //  Socket to talk to server
        Socket requester = context.socket(ZMQ.REQ);
        requester.connect("tcp://localhost:5559");
        ObjectMapper mapper = new ObjectMapper();
        Object object = mapper.readValue(new File("clientInfo.json"), Object.class);
        
        byte [] byteData = mapper.writeValueAsBytes(object);
        System.out.println("launch and connect client.");

            requester.send(byteData, 0);
            String reply = requester.recvStr(0);
            System.out.println("Received reply "  + " [" + reply + "]");
        
        //  We never get here but clean up anyhow
        requester.close();
        context.term();
    }

}
