package com.nec.connectivity.reqrepJson;

 import java.io.IOException;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ServerREPJson {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		Context context = ZMQ.context(1);

		// Socket to talk to clients
		Socket responder = context.socket(ZMQ.REP);
		responder.bind("tcp://localhost:5559");

		System.out.println("launch and connect server.");
		ObjectMapper mapper = new ObjectMapper();
		while (!Thread.currentThread().isInterrupted()) {
			// Wait for next request from client
			byte[] request = responder.recv(0);
			ClientInfo clientInfo = mapper.readValue(request, ClientInfo.class);
			System.out.println(clientInfo.getSourcePoint());
			System.out.println(clientInfo.getDestinationPoint());
			System.out.println(clientInfo.getIp());
			System.out.println(clientInfo.getClientInfo().get("name"));
//			String string = new String(request);
//			System.out.println("Received request: [" + string + "].");

			// Do some 'work'
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			// Send reply back to client
			responder.send("World".getBytes(), 0);
		}

		// We never get here but clean up anyhow
		responder.close();
		context.term();
	}

}
