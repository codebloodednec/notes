package com.nec.indexersearcher.client;

import java.io.File;
import java.io.IOException;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonReaderAndIndex {
	
	public void readAndSend() throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		ModelRoot modelRoot = mapper.readValue(new File("data.json"), ModelRoot.class);
		Context context = ZMQ.context(1);
		Socket socket = context.socket(ZMQ.REQ);
		socket.connect("tcp://localhost:5556");
		socket.send(mapper.writeValueAsBytes(modelRoot));
		System.out.println(socket.recvStr());
		socket.close();
	}
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		JsonReaderAndIndex jsonReaderAndIndex = new JsonReaderAndIndex();
		jsonReaderAndIndex.readAndSend();
	}

}
