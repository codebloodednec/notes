package com.nec.indexersearcher.client;

import java.util.List;

public class ModelRoot {
	
	private List<IndexModel> indexModel;

	public  List<IndexModel> getIndexModel() {
		return indexModel;
	}

	public void setIndexModel(List<IndexModel> indexModel) {
		this.indexModel = indexModel;
	}

}
