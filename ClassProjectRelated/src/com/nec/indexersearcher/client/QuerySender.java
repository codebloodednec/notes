package com.nec.indexersearcher.client;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

public class QuerySender {

	
	public static void main(String[] args) {
		Context context = ZMQ.context(1);
		Socket socket = context.socket(ZMQ.REQ);
		socket.connect("tcp://localhost:5557");
		socket.send("sita");
		System.out.println(socket.recvStr());
		socket.close();
		context.term();
	}
}
