package com.nec.indexersearcher.server;

import org.apache.lucene.util.SentinelIntSet;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

public class ClientToServerEntryPoint {

	public void initializeIndexAndSearchThread() throws InterruptedException {
		Runnable indexRunnable = new RunnableIndexThread();
		Runnable searchRunnable = new RunnableSearchThread();
		Thread indexThread = new Thread(indexRunnable);
		Thread searchThread = new Thread(searchRunnable);
		indexThread.start();
		searchThread.start();
		Thread.sleep(1000);
	}

	public void entryPoint() {
		// sudo tcpdump -i any -A port 5556
		Context context = ZMQ.context(1);

		// connect to indexRequest
		Socket socketIndex = context.socket(ZMQ.REP);
		socketIndex.bind("tcp://localhost:5556");
		// connect to searchRequest
		Socket socketSearch = context.socket(ZMQ.REP);
		socketSearch.bind("tcp://localhost:5557");

		Poller items = new Poller(2);
		items.register(socketIndex, ZMQ.Poller.POLLIN);
		items.register(socketSearch, ZMQ.Poller.POLLIN);

		Socket sendToIndexThread = context.socket(ZMQ.REQ);
		sendToIndexThread.connect("tcp://localhost:5558");
		Socket sendToSearchThread = context.socket(ZMQ.REQ);
		sendToSearchThread.connect("tcp://localhost:5559");
		
		System.out.println("Polling");
		while (!Thread.currentThread().isInterrupted()) {
			items.poll();
			if (items.pollin(0)) {
				System.out.println("Index polling reached");
				byte[] data = socketIndex.recv(0);
				System.out.println(data.length);
				sendToIndexThread.send(data);
				socketIndex.send(sendToIndexThread.recvStr());
			}
			if (items.pollin(1)) {
				System.out.println("Search polling reached");
				byte[] data= socketSearch.recv();
				System.out.println(data.length);
				sendToSearchThread.send(data);
				socketSearch.send(sendToSearchThread.recvStr());
			}
		}
		socketIndex.close();
		socketSearch.close();
		context.term();
	}

	public static void main(String[] args) throws InterruptedException {
		ClientToServerEntryPoint clientToServerEntryPoint = new ClientToServerEntryPoint();
		clientToServerEntryPoint.initializeIndexAndSearchThread();
		clientToServerEntryPoint.entryPoint();
	}
}