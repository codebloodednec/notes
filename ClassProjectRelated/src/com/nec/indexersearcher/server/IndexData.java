package com.nec.indexersearcher.server;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class IndexData {
	IndexWriter writer;
	Document doc;

	public IndexData() throws IOException {
		Path indexDir = Paths.get("index");
		Directory directory = FSDirectory.open(indexDir);
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig writerConfig = new IndexWriterConfig(analyzer);
		writerConfig.setOpenMode(OpenMode.CREATE_OR_APPEND);
		writer = new IndexWriter(directory, writerConfig);
	}

	public void createDocument() {
		doc = new Document();
	}

	public void index(String key, String value, String type) throws IOException {
		System.out.println(String.format("Adding key = %s, value =%s, type = %s", key, value, type));
		type = type.toLowerCase();
		switch (type) {
		case "string":
			doc.add(new StringField(key, value, Store.YES));
			break;
		case "int":
			doc.add(new NumericDocValuesField(key, Integer.valueOf(value)));
			break;
		case "text":
			doc.add(new TextField(key, value, Store.YES));
			break;
		default:
			System.out.println("Unrecognised Type");
			break;
		}
		writer.addDocument(doc);
		writer.commit();
	}
}
