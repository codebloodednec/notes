package com.nec.indexersearcher.server;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RunnableIndexThread implements Runnable {

	public void listenForData() throws JsonParseException, JsonMappingException, IOException {
		Context context = ZMQ.context(1);
		Socket socket = context.socket(ZMQ.REP);
		socket.bind("tcp://localhost:5558");
		ObjectMapper mapper = new ObjectMapper();
		IndexData indexData = new IndexData();
		System.out.println("Runnable index thread initialised.");
		while (!Thread.currentThread().isInterrupted()) {
			System.out.println("Waiting for data");
			byte[] bytes = socket.recv(0);
			System.out.println("data received at runnableindex thread");
			Map<String, Object> map = mapper.readValue(
					bytes, 
					new TypeReference<Map<String, Object>>() {
			});
			@SuppressWarnings("unchecked")
			List<Map<String, String>> datas = (List<Map<String, String>>) map.get("indexModel");
			indexData.createDocument();
			for (Map<String, String> data : datas) {
				String name = data.get("name");
				String value = data.get("value");
				String type = data.get("type");
				indexData.index(name, value, type);
				
			}
			socket.send("Successfully IndexedData");
		}
	}

	@Override
	public void run() {
		try {
			listenForData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
