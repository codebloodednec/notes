package com.nec.json;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StudentReader {
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();

		// Convert JSON string from file to Object
		Student student = mapper.readValue(new File("student.json"), Student.class);
		System.out.println(student);

		// Convert JSON string to Object
		String jsonInString = "{\"name\":\"example\",\"age\":20,\"subjects\":[\"AI\",\"maths\"]}";
		student = mapper.readValue(jsonInString, Student.class);
		System.out.println(student);

		// Pretty print
		String prettyStudent = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(student);
		System.out.println(prettyStudent);

	}
}
