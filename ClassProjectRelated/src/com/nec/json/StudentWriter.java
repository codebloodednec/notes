package com.nec.json;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StudentWriter {

	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();

		Student student = new Student();
		List<String> subjects = new ArrayList<>();
		student.setAge(20);
		student.setGrade("A");
		student.setName("example");

		subjects.add("AI");
		subjects.add("compiler");
		subjects.add("image processing");
		student.setSubjects(subjects);

		// Convert object to JSON string and save into file directly
		mapper.writeValue(new File("student.json"), student);

		// Convert object to JSON string
		String jsonInString = mapper.writeValueAsString(student);
		System.out.println(jsonInString);

		// Convert object to JSON string and pretty print
		jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(student);
		System.out.println(jsonInString);

	}

}
