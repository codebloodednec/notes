package com.nec.lucene;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class IndexSearcherApp {
	
	public void search() throws IOException, ParseException{
		Path indexDir = Paths.get("index");
		Directory directory = FSDirectory.open(indexDir);
		IndexReader indexReader = DirectoryReader.open(directory);
		IndexSearcher indexSearcher = new IndexSearcher(indexReader);
		QueryParser queryParser = new QueryParser("name", new StandardAnalyzer());
		queryParser.setAllowLeadingWildcard(true);
		Query query = queryParser.parse("*");
		TopDocs topDocs = indexSearcher.search(query,10);
        System.out.println("totalHits " + topDocs.totalHits);
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {           
            Document document = indexSearcher.doc(scoreDoc.doc);
            System.out.println("name " + document.get("name"));
//            System.out.println("age " + document.get("age"));
//            System.out.println("address " + document.get("address"));
        }
	}
	public static void main(String[] args) throws IOException, ParseException {
		IndexSearcherApp indexSearcherApp = new IndexSearcherApp();
		indexSearcherApp.search();
	}

}
