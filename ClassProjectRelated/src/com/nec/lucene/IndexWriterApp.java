package com.nec.lucene;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class IndexWriterApp {

	public void index() throws IOException {
		Path indexDir = Paths.get("index");
		Directory directory = FSDirectory.open(indexDir);
		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig writerConfig = new IndexWriterConfig(analyzer);
		writerConfig.setOpenMode(OpenMode.CREATE_OR_APPEND);
		IndexWriter writer = new IndexWriter(directory, writerConfig);
		Document doc = addDocuments("sita", 20, "maharajgunj panipokhari");
		writer.addDocument(doc);
		writer.commit();
		writer.close();
		directory.close();
	}

	public Document addDocuments(String name, int age, String address) {
		Document doc = new Document();
		doc.add(new StringField("name", name, Store.YES));
		doc.add(new NumericDocValuesField("age", age));
		doc.add(new TextField("address", address, Store.YES));
		return doc;
	}
	public static void main(String[] args) throws IOException {
		IndexWriterApp indexWriterApp = new IndexWriterApp();
		indexWriterApp.index();
	}

}
