package com.nec.mongoDB;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;

public class MongoCRUD {
	
	public static void main(String[] args) throws UnknownHostException {

	    	
//	    	// Old version, uses Mongo
	    	Mongo mongo = new Mongo("127.0.0.1", 27017);

		/**** Connect to MongoDB ****/
		// Version after 2.10.0, uses MongoClient
//		MongoClient mongo = new MongoClient("localhost", 27017);

		/**** Get database ****/
		// if database doesn't exists, MongoDB will create it for you
		DB db = mongo.getDB("newExample");

		/**** Get collection / table from 'testdb' ****/
		// if collection doesn't exists, MongoDB will create it for you
		DBCollection table = db.getCollection("userNew");

		/**** Insert ****/
		// create a document to store key and value
		BasicDBObject document = new BasicDBObject();
//		document.put("_id", "123");
		document.put("name", "sita");
		document.put("age", 20);
		document.put("createdDate", new Date());
//		List<BasicDBObject> docs = new ArrayList<BasicDBObject>();
//		docs.add(document);
		table.insert(document);
//		table.insert(docs);

		/**** Find and display ****/
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("name", "sita");

		DBCursor cursor = table.find(searchQuery);

		while (cursor.hasNext()) {
			System.out.println(cursor.next());
		}

		/**** Update ****/
		// search document where name="sita" and update it with new values
		BasicDBObject query = new BasicDBObject();
		query.put("name", "sita");

		BasicDBObject newDocument = new BasicDBObject();
		newDocument.put("name", "ram");

		BasicDBObject updateObj = new BasicDBObject();
		updateObj.put("$set", newDocument);

		table.update(query, updateObj);

		/**** Find and display ****/
		BasicDBObject searchQuery2 
		    = new BasicDBObject().append("name", "ram");

		DBCursor cursor2 = table.find(searchQuery2);

		while (cursor2.hasNext()) {
			System.out.println(cursor2.next());
		}

		/**** Done ****/
		System.out.println("Done");


	  }

}
