package com.example.instance;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({SumTest.class, TestSubtract.class})
public class AllTests {
	
}
