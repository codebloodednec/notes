package com.example.instance;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SumTest {
	
	Sum sum;
	
	@Before
	public void init(){
		sum =new Sum();
	}
	
	@Test
	public void testSum(){
//		Sum sum = new Sum();
		Assert.assertEquals(4,sum.sum(2,2));
		Assert.assertEquals(5,sum.sum(4,1));
		Assert.assertEquals(6,sum.sum(3,3));
	}
	
	@Test
	public void testConcat(){
//		Sum sum = new Sum();
		Assert.assertEquals("a and b is concatenated as ab","ab", sum.concat("a", "b"));
	}

}
