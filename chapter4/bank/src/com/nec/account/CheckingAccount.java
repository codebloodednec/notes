package com.nec.account;

public class CheckingAccount extends Account {

	public CheckingAccount(String name, int amount) {
		super(name, amount);
	}
	
	
	public void withdraw(int amount){
		super.setAmount(super.getAmount()-amount);
	}

}
