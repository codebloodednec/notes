package com.nec.account;

public class SavingsAccount extends Account{

	public SavingsAccount(String name, int amount) {
		super(name, amount);
	}
	
	public void deposit(int amount){
		super.setAmount(amount+super.getAmount());
	}
	


}
