package com.nec.application;

import com.nec.account.CheckingAccount;
import com.nec.account.SavingsAccount;

public class MainClass {

	public static void main(String[] args) {
		SavingsAccount sa =new SavingsAccount("sa", 1000);
		System.out.println(sa);
		sa.deposit(100);
		System.out.println(sa);
		CheckingAccount ca =new CheckingAccount("ca", 1000);
		System.out.println(ca);
		ca.withdraw(100);
		System.out.println(ca);

	}

}
