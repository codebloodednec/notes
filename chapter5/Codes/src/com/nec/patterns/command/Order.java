package com.nec.patterns.command;

public interface Order {
	void execute();
}
