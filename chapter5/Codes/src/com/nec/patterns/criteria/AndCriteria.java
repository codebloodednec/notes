package com.nec.patterns.criteria;

import java.util.List;

public class AndCriteria implements Criteria {
	private Criteria criteriaFirst;
	private Criteria criteriaSecond;

	public AndCriteria(Criteria criteriaFirst, Criteria criteriaSecond) {
		this.criteriaFirst = criteriaFirst;
		this.criteriaSecond = criteriaSecond;
	}

	@Override
	public List<Person> meetCriteria(List<Person> persons) {
		List<Person> firstMatchPersons = criteriaFirst.meetCriteria(persons);
		List<Person> secondMatchPersons = criteriaSecond.meetCriteria(firstMatchPersons);
		return secondMatchPersons;
	}

}
