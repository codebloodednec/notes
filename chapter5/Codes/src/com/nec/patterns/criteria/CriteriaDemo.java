package com.nec.patterns.criteria;

import java.util.ArrayList;
import java.util.List;

public class CriteriaDemo {
	
	public static void main(String[] args) {
		List<Person> persons = new ArrayList<Person>();
		Person person = new Person("ram", "male", "married");
		persons.add(person);
		person = new Person("sita", "female", "married");
		persons.add(person);
		person = new Person("hari", "male", "unmarried");
		persons.add(person);
		person = new Person("xyz", "female", "unmarried");
		persons.add(person);
		person = new Person("abc", "male", "married");
		persons.add(person);
		person = new Person("qwe", "female", "married");
		persons.add(person);
		
		Criteria criteriaMale = new CriteriaMale();
		Criteria criteriaMarried = new CriteriaMarried();
		
		Criteria andCriteria = new AndCriteria(criteriaMale, criteriaMarried);
		List<Person> marriedMale = andCriteria.meetCriteria(persons);
		
		for(Person personPrint: marriedMale){
			System.out.println(personPrint);
		}
	}

}
