package com.nec.patterns.factory;

public interface Shape {
	   void draw();
}